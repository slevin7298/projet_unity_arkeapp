﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManagerScript : MonoBehaviour {

	public string UserName;

    public InputField NameInputField;

    public Text BrjText;

    public float Credits;

    public Animator QuestCaracter;

    public int CurrentQuestID;

    public GameObject Vetements_GM;
    public GameObject Vetements_Pompiers;

    public bool LaunchQuest;

    public bool CurrentQuestOver;

    void Start () {
		
	}

    public void AssignName()
    {
        UserName = NameInputField.text;

        BrjText.text = " Bonjour " + UserName;
    }

	void Update () {
        Debug.Log(UserName);

        if (LaunchQuest && CurrentQuestOver)
        {
            LaunchQuest = false;

            StartCoroutine(QuestInquiery());
        }
	}

    public IEnumerator QuestInquiery()
    {
        CurrentQuestOver = false;

        CurrentQuestID += 1;

        if(CurrentQuestID == 1)
        {
            Vetements_GM.SetActive(true);
        }
        if (CurrentQuestID == 2)
        {
            Vetements_Pompiers.SetActive(true);
        }

        yield return new WaitForSeconds(1);

        QuestCaracter.Play("CaracterIconAppear");
    }
}