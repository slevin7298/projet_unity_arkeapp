﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialoguesController : MonoBehaviour {

    public float delay = 0.1f;

    public Text boiteDialogue;

    public Text boiteReponse;

    public bool GoToNextText;

    public bool textOver;

    public string persoFullText;

    public string reponseText;

    public string[] PhrasesPerso;

    public string[] PhrasesRéponses;

    public int currentPhrasePerso;

    public int currentPhraseReponse;

    string currentText = "...";

    public GameObject Bulle;

    public GameObject BoutonReponse;

    public AudioSource Voice;

    public GameObject All;

    public bool questEnded;

    public bool animbtn;

    public GameObject btn;

    private void Start()
    {
        if (questEnded)
        {
            StartCoroutine(StartDialogue());
        }
    }

    public void StartIt()
    {
        StartCoroutine(StartDialogue());
    }

    IEnumerator StartDialogue()
    {
        //All.GetComponent<Animator>().Play("MenuIdleMaireIsHere");

         yield return new WaitForSeconds(1);

        Bulle.SetActive(true);

        BoutonReponse.SetActive(true);

        yield return new WaitForSeconds(1);

        persoFullText = PhrasesPerso[currentPhrasePerso];

        StartCoroutine(NextCaracter());

        Voice.Play();
    }

    private void Update()
    {
        if (GoToNextText)
        {
            NextText();
        }

        if (textOver)
        {
            textOver = false;

            currentPhraseReponse += 1;

            reponseText = PhrasesRéponses[currentPhrasePerso];

            boiteReponse.text = reponseText;

            BoutonReponse.GetComponent<Animator>().Play("AnwserPopDown");

            Voice.Stop();
        }

        if(currentPhrasePerso >= PhrasesPerso.Length)
        {
            StartCoroutine(ByeText());

            Voice.Stop();

            if (questEnded)
            {
                All.GetComponent<Animator>().Play("MenuIdleMaireGoAway");
            }
        }
    }

    IEnumerator ByeText()
    {
        yield return new WaitForSeconds(1);

        Bulle.SetActive(false);

        BoutonReponse.SetActive(false);

        if (animbtn)
        {
            btn.GetComponent<Animator>().Play("BtnQueteAnim");
        }
    }

    public void NextText()
    {
        Voice.Play();

        GoToNextText = false;

        BoutonReponse.GetComponent<Animator>().Play("AnwserHideUp");

        currentPhrasePerso += 1;

        persoFullText = PhrasesPerso[currentPhrasePerso];

        StartCoroutine(NextCaracter());
    }

    IEnumerator NextCaracter()
    {
        for(int i = 0; i < persoFullText.Length; i++)
        {
            currentText = persoFullText.Substring(0, i + 1);
            boiteDialogue.text = currentText;

            if(i == persoFullText.Length -1)
            {
                textOver = true;
            }

            yield return new WaitForSeconds(delay);
        }
    }
}
